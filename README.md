The Blooming Lotus Yoga Retreat in Bali is an oasis of peace and tranquility. Firmly rooted in the classical yoga tradition we offer in-depth teacher training courses, yoga retreats, silent meditation retreats year round for both beginners and advanced students.

Address: Br. Mawang Kaja, Lodtunduh, Ubud, Bali 80571, Indonesia

Phone: +62 21 29553600

Website: https://www.blooming-lotus-yoga.com
